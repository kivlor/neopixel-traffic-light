load('api_timer.js');
load('api_neopixel.js');

let pin = 5, numPixels = 3, colorOrder = NeoPixel.GRB, i = 0;
let strip = NeoPixel.create(pin, numPixels, colorOrder);

Timer.set(1000, Timer.REPEAT, function() {
  let pixel = i++ % numPixels; 
  let color = [0, 255, 0];
  if (pixel === 1) { color = [153, 255, 0]; }
  if (pixel === 2) { color = [204, 51, 0]; }  
  
  strip.clear();
  strip.setPixel(pixel, color[0], color[1], color[2]);
  strip.show();
}, null);
